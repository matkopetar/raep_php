<?php
	require_once 'DbKonektor.php';

	// funkcija vrši upisivanje podataka o jednom komentaru u bazu podataka
	function snimiKomentar($komentar)
	{
		$rezultat = false;
		if (is_array($komentar))
		{
			try {
				$db = new DbKonektor();
				$upit = 'INSERT INTO komentari (refIdPosetilac, refIdVest, tekst)' .
						'VALUES ("'. $komentar['refIdPosetilac'] .'", "'. $komentar['refIdVest'] .
						'", "'. $komentar['tekst'] .'")';
				$rezultat = $db->upit($upit);
				if ($rezultat === false) throw new Exception("GREŠKA prilikom upisa podataka u bazu");
			} catch (Exception $e) {
				$rezultat = $e->getMessage();
			}
		}
		return $rezultat;
	}

	// funkcija vrši brisanje podataka o tačno jednom komentaru u bazi podataka
	function obrisiKomentar($idKomentar)
	{
		$rezultat = false;
		if (!empty($idKomentar))
		{
			try {
				$db = new DbKonektor();
				$upit = 'DELETE FROM komentar WHERE idKomentar='. $idKomentar .';';
				$rezultat = $db->upit($upit);
				if ($rezultat === false) throw new Exception("GREŠKA prilikom brisanja podataka iz baze");
			} catch (Exception $e) {
				$rezultat = $e->getMessage();
			}
		}
		return $rezultat;
	}

	// funkcija vrši čitanje podataka o svim komentarima u bazi podataka
	function ucitajKomentare()
	{
		$rezultat = false;
		try {
			$db = new DbKonektor();
			$upit = 'SELECT idKomentar, refIdPosetilac, refIdVest, tekst FROM komentari;';
			$rezultat = $db->upit($upit);
			if ($rezultat === false) throw new Exception("GREŠKA prilikom čitanja podataka iz baze");
		} catch (Exception $e) {
			$rezultat = $e->getMessage();
		}
		return $rezultat;
	}

	function ucitajKomentarVest () {

	$rezultat = false;
	  try {
  		$db = new DbKonektor();
  		$upit = 'SELECT komentari.idKomentar as idKomentar, '.
              'komentari.tekst as tekst, '.
              'vesti.idVest as idVest, '.
              'vesti.naslov as naslov FROM komentari INNER JOIN vesti '.
              'ON komentari.refIdVest=vesti.idVest;';
	  	$rezultat = $db->upit($upit);
  		if ($rezultat === false) throw new Exception("GREŠKA prilikom čitanja podataka iz baze");
  	} catch (Exception $e) {
  		$rezultat = $e->getMessage();
  	}
  	return $rezultat;
  }

	function ucitajKomentarPosetilac()
	{
		$rezultat = false;
		try {
			$db = new DbKonektor();
			$upit = 'SELECT posetilac.ime as ime, '.
			        'komentari.tekst as tekst, '.
					'komentari.idKomentar as idKomentar, '.
			        'posetilac.avatar as avatar FROM komentari INNER JOIN posetilac '.
					'ON komentari.refIdPosetilac=posetilac.idPosetilac ;';
			$rezultat = $db->upit($upit);
			if ($rezultat === false) throw new Exception("GREŠKA prilikom čitanja podataka iz baze");
		} catch (Exception $e) {
			$rezultat = $e->getMessage();
		}
		return $rezultat;
	}

	// funkcija vrši čitanje podataka o svim komentarima u bazi podataka i generise XML
	function ucitajKomentareXML()
	{
		$kom = new XMLWriter();
		$kom->openMemory();
		$kom->startDocument('1.0', 'UTF-8');
		$kom->startElement("ListaKomentara");

		if (is_object($rezultatCitanja = ucitajKomentare()))
		{
			//greška prilikom čitanja podataka
			$kom->startElement("greska");
				$kom->writeElement("uspesno", "true");
				$kom->writeElement("poruka", "Sve je OK");
			$kom->endElement();

			//povratni XML string sa podacima o komentarima
			$kom->startElement("lista");
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$kom->startElement("komentar");
				$kom->writeElement("idKomentar", $row->idKomentar);
				$kom->writeElement("refIdPosetilac", $row->refIdPosetilac);
				$kom->writeElement("refIdVest", $row->refIdVest);
				$kom->writeElement("tekst", $row->tekst);
				$kom->endElement();
			}
			$kom->endElement();
		}
		else
		{
			//greška prilikom čitanja podataka
			$kom->startElement("greska");
				$kom->writeElement("uspesno", "false");
				$kom->writeElement("poruka", $rezultatCitanja);
			$kom->endElement();

			//povratni XML string sa podacima o komentarima
			$kom->startElement("lista");
			$kom->endElement();
		}

		$kom->endElement();
		$kom->endDocument();
		$returnXML = $kom->outputMemory();
		echo $returnXML;
	}

	function ucitajVesti()
	{
		$rezultat = false;
		try {
			$db = new DbKonektor();
			$upit = 'SELECT * FROM vesti;';
			$rezultat = $db->upit($upit);
			if ($rezultat === false) throw new Exception("GREŠKA prilikom čitanja podataka iz baze");
		} catch (Exception $e) {
			$rezultat = $e->getMessage();
		}
		return $rezultat;
	}

	// funkcija vrši čitanje podataka o svim vestima u bazi podataka i generise XML
	function ucitajVestiXML()
	{
		$vest = new XMLWriter();
		$vest->openMemory();
		$vest->startDocument('1.0', 'UTF-8');
		$vest->startElement("ListaVesti");

		if (is_object($rezultatCitanja = ucitajVesti()))
		{
			//greška prilikom čitanja podataka
			$vest->startElement("greska");
				$vest->writeElement("uspesno", "true");
				$vest->writeElement("poruka", "Sve je OK");
			$vest->endElement();

			//povratni XML string sa podacima o vestima
			$vest->startElement("lista");
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$vest->startElement("komentar");
				$vest->writeElement("idVest", $row->idVest);
				$vest->writeElement("naslov", $row->naslov);
				$vest->writeElement("tekst", $row->tekst);
				$vest->writeElement("autor", $row->autor);
				$vest->endElement();
			}
			$vest->endElement();
		}
		else
		{
			//greška prilikom čitanja podataka
			$vest->startElement("greska");
				$vest->writeElement("uspesno", "false");
				$vest->writeElement("poruka", $rezultatCitanja);
			$vest->endElement();

			//povratni XML string sa podacima o vestima
			$vest->startElement("lista");
			$vest->endElement();
		}

		$vest->endElement();
		$vest->endDocument();
		$returnXML = $vest->outputMemory();
		echo $returnXML;
	}

	function ucitajPosetioce()
	{
		$rezultat = false;
		try {
			$db = new DbKonektor();
			$upit = 'SELECT idPosetilac, ime, email, lozinka, avatar FROM posetilac;';
			$rezultat = $db->upit($upit);
			if ($rezultat === false) throw new Exception("GREŠKA prilikom čitanja podataka iz baze");
		} catch (Exception $e) {
			$rezultat = $e->getMessage();
		}
		return $rezultat;
	}

	// funkcija vrši čitanje podataka o svim posetiocima u bazi podataka i generise XML
	function ucitajPosetioceXML()
	{
		$pos = new XMLWriter();
		$pos->openMemory();
		$pos->startDocument('1.0', 'UTF-8');
		$pos->startElement("ListaPosetilaca");

		if (is_object($rezultatCitanja = ucitajPosetioce()))
		{
			//greška prilikom čitanja podataka
			$pos->startElement("greska");
				$pos->writeElement("uspesno", "true");
				$pos->writeElement("poruka", "Sve je OK");
			$pos->endElement();

			//povratni XML string sa podacima o posetiocima
			$pos->startElement("lista");
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$pos->startElement("komentar");
				$pos->writeElement("idPosetilac", $row->idPosetilac);
				$pos->writeElement("ime", $row->ime);
				$pos->writeElement("email", $row->email);
				$pos->writeElement("lozinka", $row->lozinka);
				$pos->writeElement("avatar", $row->avatar);
				$pos->endElement();
			}
			$pos->endElement();
		}
		else
		{
			//greška prilikom čitanja podataka
			$pos->startElement("greska");
				$pos->writeElement("uspesno", "false");
				$pos->writeElement("poruka", $rezultatCitanja);
			$pos->endElement();

			//povratni XML string sa podacima o posetiocima
			$pos->startElement("lista");
			$pos->endElement();
		}

		$pos->endElement();
		$pos->endDocument();
		$returnXML = $pos->outputMemory();
		echo $returnXML;
	}

	function ucitajKomentarPosetilacXML()
	{
		$komentarPosetioca = new XMLWriter();
		$komentarPosetioca->openMemory();
		$komentarPosetioca->startDocument('1.0', 'UTF-8');
		$komentarPosetioca->startElement("ListaKometaraPosetioca");

		if (is_object($rezultatCitanja = ucitajKomentarPosetilac()))
		{
			//greška prilikom čitanja podataka
			$komentarPosetioca->startElement("greska");
				$komentarPosetioca->writeElement("uspesno", "true");
				$komentarPosetioca->writeElement("poruka", "Sve je OK");
			$komentarPosetioca->endElement();

			//povratni XML string sa podacima o komentarima posetilaca
			$komentarPosetioca->startElement("lista");
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$komentarPosetioca->startElement("komentarPosetioca");
				$komentarPosetioca->writeElement("ime", $row->ime);
				$komentarPosetioca->writeElement("idKomentar", $row->idKomentar);
				$komentarPosetioca->writeElement("tekst", $row->tekst);
				$komentarPosetioca->writeElement("avatar", $row->avatar);
				$komentarPosetioca->endElement();
			}
			$komentarPosetioca->endElement();
		}
		else
		{
			//greška prilikom čitanja podataka
			$komentarPosetioca->startElement("greska");
				$komentarPosetioca->writeElement("uspesno", "false");
				$komentarPosetioca->writeElement("poruka", $rezultatCitanja);
			$komentarPosetioca->endElement();

			//povratni XML string sa podacima o komentarima posetilaca
			$komentarPosetioca->startElement("lista");
			$komentarPosetioca->endElement();
		}

		$komentarPosetioca->endElement();
		$komentarPosetioca->endDocument();
		$returnXML = $komentarPosetioca->outputMemory();
		echo $returnXML;
	}

	function ucitajKomentarVestXML()
	{
		$komentarVesti = new XMLWriter();
		$komentarVesti->openMemory();
		$komentarVesti->startDocument('1.0', 'UTF-8');
		$komentarVesti->startElement("ListaKometaraVesti");

		if (is_object($rezultatCitanja = ucitajKomentarVest()))
		{
			//greška prilikom čitanja podataka
			$komentarVesti->startElement("greska");
				$komentarVesti->writeElement("uspesno", "true");
				$komentarVesti->writeElement("poruka", "Sve je OK");
			$komentarVesti->endElement();

			//povratni XML string sa podacima o komentarima vesti
			$komentarVesti->startElement("lista");
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$komentarVesti->startElement("komentarVesti");
				$komentarVesti->writeElement("idKomentar", $row->idKomentar);
				$komentarVesti->writeElement("tekst", $row->tekst);
				$komentarVesti->writeElement("idVest", $row->idVest);
				$komentarVesti->writeElement("naslov", $row->naslov);
				$komentarVesti->endElement();
			}
			$komentarVesti->endElement();
		}
		else
		{
			//greška prilikom čitanja podataka
			$komentarVesti->startElement("greska");
				$komentarVesti->writeElement("uspesno", "false");
				$komentarVesti->writeElement("poruka", $rezultatCitanja);
			$komentarVesti->endElement();

			//povratni XML string sa podacima o komentarima vesti
			$komentarVesti->startElement("lista");
			$komentarVesti->endElement();
		}

		$komentarVesti->endElement();
		$komentarVesti->endDocument();
		$returnXML = $komentarVesti->outputMemory();
		echo $returnXML;
	}


	// funkcija vrši čitanje podataka o svim komentarima u bazi podataka i generise JSON
	/*
	function ucitajKomentareJSON()
	{
		$root = new stdClass();
		$data = new stdClass();

		if (is_object($rezultatCitanja=ucitajKomentare()))
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = true;
				$data->greska->poruka = "Sve je OK";

			//niz sa podacima o komentarima
			$data->lista = array();
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$kom1 = new stdClass();
					$kom1->idKomentar = $row->idKomentar;
					$kom1->ime = $row->ime;
					$kom1->tekst = $row->tekst;
					$kom1->ocena = $row->ocena;
				$data->lista[] = $kom1;
			}
		}
		else
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = false;
				$data->greska->poruka = $rezultatCitanja;

			//niz sa podacima o komentarima
			$data->lista = array();
		}

		$root->ListaKomentara = $data;
		$returnJSON = json_encode($root);
		echo $returnJSON;
	} */

	function ucitajKomentareJSON()
	{
		$root = new stdClass();
		$data = new stdClass();

		if (is_object($rezultatCitanja=ucitajKomentare()))
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
			$data->greska->uspesno = true;
			$data->greska->poruka = "Sve je OK";

			//niz sa podacima o komentarima
			$data->lista = array();
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$kom1 = new stdClass();

					$kom1->idKomentar = $row->idKomentar;
					$kom1->refIdPosetilac = $row->refIdPosetilac;
					$kom1->refIdVest = $row->refIdVest;
					$kom1->tekst = $row->tekst;


				$data->lista[] = $kom1;
			}
		}
		else
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = false;
				$data->greska->poruka = $rezultatCitanja;

			//niz sa podacima o komentarima
			$data->lista = array();
		}

		$root->ListaKomentara = $data;
		$returnJSON = json_encode($root);
		echo $returnJSON;
	}

	function ucitajVestiJSON()
	{
		$root = new stdClass();
		$data = new stdClass();

		if (is_object($rezultatCitanja=ucitajVesti()))
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = true;
				$data->greska->poruka = "Sve je OK";

			//niz sa podacima o komentarima
			$data->lista = array();
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$vest1 = new stdClass();
					$vest1->idVest = $row->idVest;
					$vest1->naslov = $row->naslov;
					$vest1->tekst = $row->tekst;
					$vest1->autor = $row->autor;

				$data->lista[] = $vest1;
			}
		}
		else
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = false;
				$data->greska->poruka = $rezultatCitanja;

			//niz sa podacima o komentarima
			$data->lista = array();
		}

		$root->ListaVesti = $data;
		$returnJSON = json_encode($root);
		echo $returnJSON;
	}


	function ucitajPosetioceJSON()
	{
		$root = new stdClass();
		$data = new stdClass();

		if (is_object($rezultatCitanja=ucitajPosetioce()))
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = true;
				$data->greska->poruka = "Sve je OK";

			//niz sa podacima o komentarima
			$data->lista = array();
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$posetilac1 = new stdClass();
					$posetilac1->idPosetilac = $row->idPosetilac;
					$posetilac1->ime = $row->ime;
					$posetilac1->email = $row->email;
					$posetilac1->lozinka = $row->lozinka;
					$posetilac1->avatar = $row->avatar;
				$data->lista[] = $posetilac1;
			}
		}
		else
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = false;
				$data->greska->poruka = $rezultatCitanja;

			//niz sa podacima o komentarima
			$data->lista = array();
		}

		$root->ListaPosetioca = $data;
		$returnJSON = json_encode($root);
		echo $returnJSON;
	}



	function ucitajKomentarVestJSON()
	{
		$root = new stdClass();
		$data = new stdClass();

		if (is_object($rezultatCitanja=ucitajKomentarVest()))
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = true;
				$data->greska->poruka = "Sve je OK";

			//niz sa podacima o komentarima
			$data->lista = array();
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$komVest = new stdClass();
					$komVest->idKomentar = $row->idKomentar;
					$komVest->tekst = $row->tekst;
					$komVest->idVest = $row->idVest;
					$komVest->naslov = $row->naslov;
				$data->lista[] = $komVest;
			}
		}
		else
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = false;
				$data->greska->poruka = $rezultatCitanja;

			//niz sa podacima o komentarima
			$data->lista = array();
		}

		$root->ListaKomentarVest = $data;
		$returnJSON = json_encode($root);
		echo $returnJSON;
	}



	function ucitajKomentarPosetilacJSON()
	{
		$root = new stdClass();
		$data = new stdClass();

		if (is_object($rezultatCitanja=ucitajKomentarPosetilac()))
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = true;
				$data->greska->poruka = "Sve je OK";

			//niz sa podacima o komentarima
			$data->lista = array();
			while ($row = mysqli_fetch_object($rezultatCitanja))
			{
				$komPoset = new stdClass();
					$komPoset->ime = $row->ime;
					$komPoset->idKomentar = $row->idKomentar;
					$komPoset->tekst = $row->tekst;
					$komPoset->avatar = $row->avatar;
				$data->lista[] = $komPoset;
			}
		}
		else
		{
			//greška prilikom čitanja podataka
			$data->greska = new stdClass();
				$data->greska->uspesno = false;
				$data->greska->poruka = $rezultatCitanja;

			//niz sa podacima o komentarima
			$data->lista = array();
		}

		$root->ListaKomentarPosetilac = $data;
		$returnJSON = json_encode($root);
		echo $returnJSON;
	}
?>
