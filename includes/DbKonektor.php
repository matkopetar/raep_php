<?php

require_once 'SistemPromenljive.php';

class DbKonektor extends SistemPromenljive {
	private $upit;
	private $link;

	public function __construct() {
		$sistemskep = SistemPromenljive::getSistemPromenljive();

		$host = $sistemskep['host'];
		$korisnik = $sistemskep['korisnik'];
		$lozinka = $sistemskep['lozinka'];
		$baza = $sistemskep['baza'];

		$this->link = mysqli_connect($host, $korisnik, $lozinka,$baza);
		if( ($this->link) == false )
			throw new Exception("GREŠKA prilikom konekcije na server");

		$uspesno = mysqli_select_db($this->link, $baza);
		if( !$uspesno )
			throw new Exception("GREŠKA prilikom selektovanja baze");

		mysqli_set_charset($this->link, "UTF8");

		register_shutdown_function(array(&$this, 'close'));
	}

	public function upit($upitStr) {
		$this->upit = $upitStr;
		return mysqli_query($this->link, $upitStr);
	}

	public function fetchArray($result) {
		return mysqli_fetch_array($result);
	}

	public function close() {

   //	return mysqli_close($this->link);
	}

	function __destruct() {
	  $this->close();
	    }

}


?>
