<?php

  require_once '../includes/DbKonektor.php';
  require_once 'userKlasa.php';

  if(isset($_POST['register'])) {
    $ime = $_POST['ime'];
    $email = $_POST['email'];
    $lozinka = $_POST['lozinka'];

    $db = new DbKonektor();
    $user = new User();
    if ($user->register($ime,$email,$lozinka)) {  //Registuje korisnika
      echo "Vaš nalog je uspešno kreiran";
    }
  }
?>

<!DOCTYPE html>
<html>
 <head>
   <meta http-equiv="content-type" content="text/html; charset=utf-8" />
   <title></title>
   <link href="../stil.css" rel="stylesheet" type="text/css" />
 </head>
 <body style="background: grey; color: white;">

    <form class="forma" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
      <label for="ime">Ime: </label>
      <input type="text" name="ime"/><br />
      <label for="email">Email: </label>
      <input type="text" name="email" /><br />
      <label for="lozinka">Lozinka: </label>
      <input type="password" name="lozinka" /><br />
      <input type="submit" name="register" value="Registruj se" />
    </form>

 </body>
</html>
