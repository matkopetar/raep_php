<?php

require_once '../includes/DbKonektor.php';

class User
{

    public function register($ime,$email,$lozinka)
    {
       try
       {
           $db = new DbKonektor(); //Pravimo objekat konektor baze
           $hash = hash('sha256',$lozinka); //Hash-ujem lozinku
           $upit = "INSERT INTO posetilac(ime,email,lozinka) VALUES('$ime', '$email', '$hash')"; //upit ubacivanja podataka korisnika koji treba da se registruje u bazu
           $rezultat = $db->upit($upit);  //upisujemo podatke u bazu
           return $rezultat;  //vraca true ukoliko je uspesno upisao podatke u bazu
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }
    }

    public function login($email,$lozinka)
    {
       try
       {
          $db = new DbKonektor(); //Pravimo objekat konektor baze
          $hash = hash('sha256', $lozinka); //Hash-ujem lozinku
          $upit = "SELECT * FROM posetilac WHERE email='$email' AND lozinka='$hash' ";  //Trazimo podatke iz baze ako se poklapa email i lozinka
          $rezultat = $db->upit($upit);  
          if(mysqli_num_rows($rezultat) > 0)  //Ukoliko su lozinka i email ispravni Ulogujemo korisnika
          {
                $row = $db->fetchArray($rezultat);
                $_SESSION['idPosetilac'] = $row['idPosetilac'];
                return true;
          }
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }
   }

   // Proverava da li je korisnik ulogovan
   public function is_loggedin()
   {
      if(isset($_SESSION['idPosetilac']))
      {
         return true;
      }
   }

   // Preusmerava korisnika na odredjen url
   public function redirect($url)
   {
       header("Location: $url");
   }

}
?>
