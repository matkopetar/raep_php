<?php

  require_once 'userKlasa.php';
  require_once '../includes/DbKonektor.php';
	session_start();

  $error = "";
  $user = new User();
  
  //Proverava da li je korisnik ulogovan, ako jeste preusmerava ga na index.php(pocetnu)
  if($user->is_loggedin()) {
    $user->redirect('../index.php');
  }
  
  // Uloguje korisnika ako su podaci ispravni
  if(isset($_POST['login']))
  {
    $email = $_POST['email'];
    $lozinka = $_POST['lozinka'];

    if($user->login($email,$lozinka))
    {
      if (isset($_SESSION['idVest']))
      {;
         $user->redirect('../vesti/vestSaPrikazanimKomentarima.php?id='.$_SESSION['idVest']);      
       }
       else
       {
         $user->redirect('../index.php');
       }
     }
    else
    {
      $error = "E-mail ili lozinka nisu ispravni.";
    }
  }
?>
<html>
 <head>
   <meta http-equiv="content-type" content="text/html; charset=utf-8" />
   <title></title>
   <link href="../stil.css" rel="stylesheet" type="text/css" />
 </head>
 <body style="background: grey; color: white;">

    <form class="forma" method="post" action="login.php">
      <label for="email">Email: </label>
      <input type="text" name="email" /><br name="idVest" value="2" />
      <label for="lozinka">Lozinka: </label>
      <input type="password" name="lozinka" /><br />
      <input type="submit" name="login" value="Log In" />
    </form>

    <p id="linkIspodForme"><a href="register.php">Nemaš nalog?! Registruj se sada!</a></p>
    <p><?php echo $error;?></p>
  </body>
</html>
