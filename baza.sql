USE test;

CREATE TABLE IF NOT EXISTS `komentari` (
  `idKomentar` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `refIdPosetilac` int(10) NOT NULL,
  `refIdVest` int(10) NOT NULL,
  `tekst` text,
  `vremeKreiranja` datetime DEFAULT NOW(),
  PRIMARY KEY (`idKomentar`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

CREATE TABLE IF NOT EXISTS `posetilac` (
	`idPosetilac` int(10) NOT NULL AUTO_INCREMENT,
	`ime` text NOT NULL,
	`email` text NOT NULL,
	`lozinka` char(64) NOT NULL,
	`avatar` text,
	PRIMARY KEY (`idPosetilac`)
);

INSERT INTO `posetilac` (idPosetilac, ime, email, lozinka) VALUES
(1,'Petar','petar.petrovic@gmail.com','65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5'),
(2, 'Nikola','nikola.nikolic@gmail.com','65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5'),
(3, 'Filip', 'filip.filipovic@live.com','65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5');

/*65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5 -> lozinka je "qwerty"*/

CREATE TABLE IF NOT EXISTS `vesti` (
	`idVest` int(10) NOT NULL AUTO_INCREMENT,
	`naslov` text NOT NULL,
	`tekst` text NOT NULL,
	`sTekst` char(55), /* skracen tekst od 55 karaktera za prikaz na home page-u */
	`vremeKreiranja` datetime DEFAULT NOW(),
	`autor` text NOT NULL,
	PRIMARY KEY (`idVest`)
);

INSERT INTO `vesti` (idVest, naslov, tekst, sTekst, autor) VALUES
(1,'Kafa','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing el..','Kurir'),
(2,'Prosecan covek u Srbiji dnevno popusi 16 cigareta','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing el..','Informer'),
(3,'Psi lutalice','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing el..','Blic');
