<?php
require_once('/../includes/DbKonektor.php');

  class Komentar{
    private $idKomentar = null;
    private $refIdPosetilac = null;
    private $refIdVest = null;
    private $tekst = null;
    private $vremeKreiranja = null;


    // konstraktor klase, od koristi je u vestKlasa.php ( kada se iz baze pravi lista komentara i vraca funkcijom vratiSveKomentare)
    function __construct ($idKomentar, $refIdPosetilac, $refIdVest, $tekst, $vremeKreiranja)
    {
      $this->idKomentar = $idKomentar;
      $this->refIdPosetilac = $refIdPosetilac;
      $this->refIdVest = $refIdVest;
      $this->tekst = $tekst;
      $this->vremeKreiranja = $vremeKreiranja;
    }

    //get funkcije
    public function vratiIdKomentara(){
      return $this->idKomentar;
    }
    public function vratiRefIdPosetioca(){
      return $this->refIdPosetilac;
    }
    public function vratiRefIdVest(){
      return $this->refIdVest;
    }
    public function vratiTekst(){
      return $this->tekst;
    }
    public function vratiVremeKreiranja(){
      return $this->vremeKreiranja;
    }

   public function vratiImeAutora(){ //vraca ime autora po id-u komentara
     $db = new DbKonektor();

     $upit = "SELECT ime FROM posetilac WHERE idPosetilac ='$this->refIdPosetilac'"; //upit koji vraca red iz klase autor za id koji je u komentaru upisan u refIdPosetilac

     $rezultat = $db->upit($upit);

     $posetilac = $db->fetchArray($rezultat); //hvata dati red

     unset($db);
     return $posetilac["ime"]; //vraca ime


   }
  }



 ?>
