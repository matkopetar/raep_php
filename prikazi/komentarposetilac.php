<?php
require_once('../includes/funkcije.php');

if( isset($_GET['xml']) ) {				//za prikazi/komentarposetilac.php?xml
	if( isset($_GET['download']) ) {  //za prikazi/komentarposetilac.php?xml&download
		header('Content-disposition: attachment; filename="komentarposetilac.xml"');
		header('Content-type: "text/xml"; charset="utf8"');
	}
	echo ucitajKomentarPosetilacXML();
}

if( isset($_GET['json']) ) {    //za prikazi/komentarposetilac.php?json
	if( isset($_GET['download']) ) {  //za prikazi/komentarposetilac.php?json&download
		header('Content-disposition: attachment; filename=komentarposetilac.json');
		header('Content-type: application/json');
	}
	echo ucitajKomentarPosetilacJSON();
}
?>
