<?php
require_once('../includes/funkcije.php');

if( isset($_GET['xml']) ) {				//za prikazi/komentari.php?xml
	if( isset($_GET['download']) ) {  //za prikazi/komentari.php?xml&download
		header('Content-disposition: attachment; filename="komentari.xml"');
		header('Content-type: "text/xml"; charset="utf8"');
	}
	echo ucitajKomentareXML();
}

if( isset($_GET['json']) ) {    //za prikazi/komentari.php?json
	if( isset($_GET['download']) ) { //za prikazi/komentari.php?json&download
		header('Content-disposition: attachment; filename=komentari.json');
		header('Content-type: application/json');
	}
	echo ucitajKomentareJSON();
}
?>
