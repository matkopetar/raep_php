<?php
require_once('../includes/funkcije.php');

if( isset($_GET['xml']) ) {
	if( isset($_GET['download']) ) {
		header('Content-disposition: attachment; filename="vesti.xml"');
		header('Content-type: "text/xml"; charset="utf8"');
	}
	echo ucitajVestiXML();
}

if( isset($_GET['json']) ) {
	if( isset($_GET['download']) ) {	
		header('Content-disposition: attachment; filename=vesti.json');
		header('Content-type: application/json');
	}
	echo ucitajVestiJSON();
}
?>
