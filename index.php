<?php 
require_once('vesti/vestKlasa.php');
session_start();

if (isset($_SESSION['idPosetilac']))
{
  echo "<a href=\"logout.php\">Log Out</a>";
}
else 
{
  echo "<a href=\"posetioci/login.php\">Log In</a>";
}
?> 
 <html>
 	<head>
 		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
 		<title></title>
 		<link href="stil.css" rel="stylesheet" type="text/css" />
 	</head>
 	<body style="background: grey; color: white;">
 		<div class='vesti' style="text-align: center">

<?php

  $vestiObjekat = new Vest(); //objekat vest
  $sveVesti = $vestiObjekat->ucitajSveVesti(); //poziva member funkciju objekta vest koja vraca array svih vesti( select iz baze pretvara u objekte )

  foreach ($sveVesti as &$vest) { //stampa svaku vest
    echo("<div class='vest'>");
    echo("<h1 class='naslovVesti'>".$vest->vratiNaslov()."</h1>");
    echo("<p class='sTekst'>".$vest->vratiSTekst()."</p>");
    echo("<p class='imeAutora'>".$vest->vratiAutora()."<br />".$vest->vratiVremeKreiranja()."</p>");
    echo("<div class ='dugme'><a href='vesti/vest.php?id=".$vest->vratiIdVest()."'>Prikazi</a></div>"); //prosledjuje id fajlu vesti/vest.php, gde se objekat ponovo generise preko id-a ( member funkcijom ucitajVestPoIdu)
    echo("</div>");
  }

?>
   </div>
  </body>


</html>
