<?php
require_once('vestKlasa.php');
$vestKonektor= new Vest();
session_start();

if (isset($_GET['id']))
{
  $idVest = $_GET['id'];
  $vest = $vestKonektor->ucitajVestPoIdu($idVest);
  //generise se objekat tipa vest sa svim svojstvima koje ima u bazi po idu koji sam prosledio sa strane index.html
}

if (isset($_SESSION['idPosetilac']))
{
  echo "<a href=\"../logout.php\">Log Out</a>";
}

?>
<html>
 <head>
   <meta http-equiv="content-type" content="text/html; charset=utf-8" />
   <title></title>
   <link href="../stil.css" rel="stylesheet" type="text/css" />
 </head>
 <body style="background: grey; color: white;">
   <div class='vesti' style="text-align: center">
<?php
  // prikaz jedne vesti
  echo("<div class='vest'>");
  echo("<h1 class='naslovVesti'>".$vest->vratiNaslov()."</h1>");
  echo("<p class='sTekst'>".$vest->vratiTekst()."</p>");
  echo("<p class='imeAutora'>".$vest->vratiAutora()."<br />".$vest->vratiVremeKreiranja()."</p>");
  echo("<div class ='dugme'><a href='vestSaPrikazanimKomentarima.php?id=".$vest->vratiIdVest()."'>Prikazi komentare</a></div>");
  echo("</div>");


 ?>
    </div>
   </body>


 </html>
