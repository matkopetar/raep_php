<?php
require_once('/../includes/DbKonektor.php');
require_once('/../komentari/komentarKlasa.php');


class Vest {

  private $naslov = null;
  private $vremeKreiranja = null;
  private $sTekst = null;
  private $tekst = null;
  private $idVest = null;
  private $autor = null;
  //svojstva koja su paralela onima iz tabele

  public function postaviSvojstva($naslov,$vremeKreiranja,$tekst,$sTekst,$idVest,$autor) //funkcija koja se ponasa kao konstruktor
  {
    $this->naslov = $naslov;
    $this->vremeKreiranja = $vremeKreiranja;
    $this->tekst = $tekst;
    $this->sTekst = $sTekst;
    $this->idVest = $idVest;
    $this->autor = $autor;
  }
  public function ucitajSveVesti() {
    //db ucitaj
    //napravi objekte
    //ucitaj ih u dictionary


    $db = new DbKonektor();

    $upitUcitavanja = "SELECT * FROM vesti"; //upit koji ce povuci sve vesti iz baze

    $rezultatUpita = $db->upit($upitUcitavanja);

    // prelazi red po red po rezultatu upita dok ne istrosi svaki red u rezultatu
    while($trenutnaVest = $db->fetchArray($rezultatUpita)){
    //upisuje u objekat svojstva reda iz baze
      $obj = new Vest();
      $obj->postaviSvojstva($trenutnaVest["naslov"],
                            $trenutnaVest["vremeKreiranja"],
                            $trenutnaVest["tekst"],
                            $trenutnaVest["sTekst"],
                            $trenutnaVest["idVest"],
                            $trenutnaVest["autor"]
                          );
      $vesti[] = $obj; //pushuje u array $vesti svaki objekat
      //upisao je sve atribute iz rezultata u privremeni objekat OBJ i upisao to u array vesti (push)
    }
    unset($db);

    return $vesti;
     //vraca array vesti
  }

  public function ucitajVestPoIdu($idZeljenogClanka) {
  //logika za vracanje objekta po id-u (search u bazi)
  //blablatruc
    $db = new DbKonektor();
    $upitUcitavanja = "SELECT * FROM vesti WHERE idVest = '$idZeljenogClanka'"; //upit, trazim vest odredjenog id-a
    $rezultatUpita = $db->upit($upitUcitavanja);
    $trazenaVest = $db->fetchArray($rezultatUpita);

    $vest = new Vest();
    $vest->postaviSvojstva($trazenaVest["naslov"],
                           $trazenaVest["vremeKreiranja"],
                           $trazenaVest["tekst"],
                           $trazenaVest["sTekst"],
                           $trazenaVest["idVest"],
                           $trazenaVest["autor"]
                         );
  //napravi objekat vest koji cu vratiti na kraju funkcije

    unset($db);
  //zatvaram konekciju
     //ovo je u slucaju da je prazan array vesti
    return $vest;

  }

  public function ucitajSveKomentare() {
  //db ucitaj
  //napravi objekte
  //ucitaj ih u dictionary


    $db = new DbKonektor();

    $upitUcitavanja = "SELECT * FROM komentari WHERE  refIdVest='$this->idVest'"; //upit, trazim sve komentare povezane sa konkretnom vescu

    $rezultatUpita = $db->upit($upitUcitavanja);

    // prelazi red po red po rezultatu upita dok ne istrosi svaki red u rezultatu
    while($trenutniKomentar = $db->fetchArray($rezultatUpita)){
      //upisujem sve komentare (kao objekte) u array $komentari
      $obj = new Komentar($trenutniKomentar["idKomentar"],
                          $trenutniKomentar["refIdPosetilac"],
                          $trenutniKomentar["refIdVest"],
                          $trenutniKomentar["tekst"],
                          $trenutniKomentar["vremeKreiranja"]);
    //objekat je novi komentar, pravi se sa rezultatima upita, salje u array (push)

    $komentari[] = $obj; //pushuje u array
    //upisao je sve atribute iz rezultata u privremeni objekat OBJ i upisao to u array vesti (push)
    }
    unset($db);
     //zatvaram konekciju
    //u slucaju da je prazan array komentari, vraca null, u suprotnom, vraca komentare
    if(isset($komentari))
    {
      return $komentari;
    }
    else
    {
      return null;
    }

   //vraca array komentara
  }

  //vraca vrednosti privatnih svojstava objekta Vest
  public function vratiNaslov(){
    return $this->naslov;
  }
  public function vratiVremeKreiranja(){
    return $this->vremeKreiranja;
  }
  public function vratiSTekst(){
    return $this->sTekst;
  }
  public function vratiTekst(){
    return $this->tekst;
  }
  public function vratiIdVest(){
    return $this->idVest;
  }
  public function vratiAutora(){
    return $this->autor;
  }


}


?>
