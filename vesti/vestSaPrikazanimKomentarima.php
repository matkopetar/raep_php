<?php
require_once('vestKlasa.php');
require_once('/../komentari/komentarKlasa.php');
session_start();

if (isset($_SESSION['idPosetilac']))
{
  echo "<a href=\"../logout.php\">Log Out</a>";
}

if (isset($_GET['id']))
{
  $vestKonektor= new Vest();
  $idVest = $_GET['id'];
  $vest = $vestKonektor->ucitajVestPoIdu($idVest);
}
?>
<html>
 <head>
   <meta http-equiv="content-type" content="text/html; charset=utf-8" />
   <title></title>
   <link href="../stil.css" rel="stylesheet" type="text/css" />
 </head>
 <body style="background: grey; color: white;">
   <div class='vesti' style="text-align: center">
<?php

  echo("<div class='vest'>");
  echo("<h1 class='naslovVesti' >".$vest->vratiNaslov()."</h1>");
  echo("<p class='sTekst' >".$vest->vratiTekst()."</p>");
  echo("<p class='imeAutora'>".$vest->vratiAutora()."<br />".$vest->vratiVremeKreiranja()."</p>");
  echo("</div>");
?>
   </div>
<form class='forma' method="post" action="dodajKomUBazu.php?id=<?php echo($vest->vratiIdVest()); ?>">
  <input id="tekstPolje" type="text" name="tekst"  />
  <input id="submitDugme" type="submit" name="dodajKomentar" value="Dodaj Komentar"/>
</form>

<?php
  echo("<div class='komentari'>");
  $komentari = $vest->ucitajSveKomentare();
  if (isset($komentari)){
  foreach($komentari as &$komentar){
    echo("<div class='komentar'>");
    echo("<p class='imeAutoraKomentara'>".$komentar->vratiImeAutora()."</p>");
    echo("<p class='tekstKomentara'>".$komentar->vratiTekst()."</p>");
    echo("</div>");

  }

}
  echo("</div>");

 ?>

   </body>


 </html>
